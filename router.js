import {
  createStackNavigator,
  createAppContainer
} from 'react-navigation';
import Home from './screen/home';
import People from './screen/people';

const Router = createStackNavigator({
  Home: Home,
  verification: People
});

const App = createAppContainer(Router);

export default App;
